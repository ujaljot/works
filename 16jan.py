#creating class
class Employee:
    #data member
    branch="SE"
    company = "sachtech private"
    #member function
    def info(anb):
        print("member function")

#creating object of a class
ob = Employee()

#accessing data member
print(ob.company)

#accessing member function
ob.info()




class Student:
    branch = "CSE"
    college = "abcd engineering college"

    def information(self,name,rno,email):
        print("name: {} \n Roll no: {} \n Email: {}".format(name,rno,email))
        print("**********************\n")

ob = Student()
ob2 = Student()
print(ob.college, "({})".format(ob.branch))
ob.information("aman",1,"aman@gmail.com")
ob2.information("harry",2,"harry@gmail.com")